<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 12:38
 */

namespace Generator;

use Entities\PageEntity;
use Entities\UrlEntity;
use Exceptions\IncorrectEntityInstanceException;
use Services\ContentLoaderService;
use Services\DatabaseService;
use Services\ParserService;

class GenerateMediator
{
    const WORKING_TIME = 1800;
    const STATUS_NEW = 'new';
    const STATUS_SUCCESS = 'success';
    const STATUS_IN_PROGRESS = 'in_progress';

    private $databaseService;

    public function __construct()
    {
        $this->databaseService = new DatabaseService();
    }

    public function mediate()
    {
        echo 'start' . PHP_EOL;
        $urlEntityForTaskArray = $this->databaseService->getUrlForTask();
        foreach ($urlEntityForTaskArray as $task) {
            if ($task->status === self::STATUS_NEW) {
                $this->databaseService->createTableForParse($task);
                $startTime = microtime(true);
                while (($firstUrl=$this->databaseService->getUrl())&&(microtime(true)<$startTime+self::WORKING_TIME)) {
                    if (!$firstUrl->isVirtual()) {
                        echo 'Working with: ' . $firstUrl->url . PHP_EOL;

                        if (!$firstUrl->checkHttpResponseCode()) {
                            $this->databaseService->updateUrlStatus($firstUrl, UrlEntity::STATUS_IGNORE);
                            continue;
                        }
                        $this->databaseService->updateUrlStatusForTask($task->id, self::STATUS_IN_PROGRESS);

                        $contentLoaderService = new ContentLoaderService($firstUrl);

                        $parserService = new ParserService($contentLoaderService->getResult(), $task);
                        $parserService->parseContent();
                        $this->databaseService->saveUrls($parserService->getResult());
                        $this->databaseService->updateUrlStatus($firstUrl, UrlEntity::STATUS_SUCCESS);
                        $this->databaseService->updateUrlStatusForTask($task->id, self::STATUS_SUCCESS);
                    }
                }
            }
        }
        echo 'finish' . PHP_EOL;
    }
}
