CREATE DATABASE IF NOT EXISTS `generator`;

CREATE TABLE IF NOT EXISTS `parse_sitemap_tasks` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`domain` VARCHAR(255) NOT NULL DEFAULT '0',
	`first_page` VARCHAR(255) NOT NULL DEFAULT '0',
	`protocol` VARCHAR(10) NOT NULL DEFAULT '0',
	`status` ENUM('new','success','in_progress','finish') NOT NULL DEFAULT 'new',
	`host` VARCHAR(50) NOT NULL DEFAULT '0',
	`port` INT(6) NOT NULL DEFAULT '0',
	`username` VARCHAR(255) NOT NULL DEFAULT '0',
	`password` VARCHAR(255) NOT NULL DEFAULT '0',
	`root` VARCHAR(255) NOT NULL DEFAULT '0',
	`table_name_for_urls` VARCHAR(50) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci';
