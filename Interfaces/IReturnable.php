<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 12:08
 */

namespace Interfaces;

interface IReturnable
{
    public function getResult();
}