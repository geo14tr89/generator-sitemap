<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 11:55
 */

namespace Entities;

class PageEntity
{
    public $url;
    public $content;

    public function __construct($url, $content)
    {
        $this->url = $url;
        $this->content = $content;
    }
}
