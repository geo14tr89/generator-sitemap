<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 04.01.2019
 * Time: 12:34
 */

namespace Entities;

class UrlEntityForTask
{
    const URL_SLASH = '/';

    public $id;
    public $domain;
    public $first_page;
    public $protocol;
    public $status;
    public $table_name_for_urls;

    public function __construct($id, $domain, $first_page, $protocol, $status, $table_name_for_urls)
    {
        $this->id = $id;
        $this->domain = $domain;
        $this->first_page = $first_page;
        $this->protocol = $protocol;
        $this->status = $status;
        $this->table_name_for_urls = $table_name_for_urls;
    }

    public function getDomainWithProtocol()
    {
        return $this->protocol . $this->domain;
    }

    public function getDomainAndProtocolWithSlash()
    {
        return $this->protocol . $this->domain . self::URL_SLASH;
    }
}
