<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 12:01
 */

namespace Entities;

class UrlEntity
{
    const STATUS_NEW = 'new';
    const STATUS_WORKING = 'working';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';
    const STATUS_IGNORE = 'ignore';
    const RESPONSE_CODE_ERROR = 404;

    public $id;
    public $url;
    public $status;

    protected $parsedUrl;

    public function __construct($url, $id = null, $status = null)
    {
        $this->id = $id;
        $this->url = $url;
        $this->status = $status;
        $this->parse();
        $this->clearUrl();
        $this->removeTel();
        $this->removeMail();
    }

    private function parse()
    {
        $this->parsedUrl = parse_url($this->url);
    }

    public function clearUrl()
    {
        $this->url = $this->parsedUrl['scheme'] . '://' . $this->parsedUrl['host'] . $this->parsedUrl['path'];
    }

    public function removeTel()
    {
        if (strpos($this->url, 'tel:') !== false) {
            $this->url = $this->parsedUrl['scheme'] . '://' . $this->parsedUrl['host'] . '/';
        }
    }

    public function removeMail()
    {
        if (strpos($this->url, 'mailto:') !== false) {
            $this->url = $this->parsedUrl['scheme'] . '://' . $this->parsedUrl['host'] . '/';
        }
    }

    public function checkHttpResponseCode()
    {
        $headers = get_headers($this->url);
        $response_code = substr($headers[0], 9, 3);
        return (($response_code === (string) self::RESPONSE_CODE_ERROR) ? false : true);
    }

    public function isVirtual()
    {
        return (!$this->id);
    }
}
