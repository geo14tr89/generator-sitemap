<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 12:02
 */

namespace Services;

use Entities\PageEntity;
use Entities\UrlEntity;
use Interfaces\IReturnable;

class ContentLoaderService implements IReturnable
{
    protected $urlEntity;
    protected $content;

    public function __construct(UrlEntity $url)
    {
        $this->urlEntity = $url;
        $this->loadContent();
    }

    public function loadContent()
    {
        $ch = curl_init($this->urlEntity->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $this->content = curl_exec($ch);
        curl_close($ch);
    }

    public function getResult()
    {
        // TODO: Implement getResult() method.
        return new PageEntity($this->urlEntity->url, $this->content);
    }
}
