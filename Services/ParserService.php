<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 12:03
 */

namespace Services;

use Entities\PageEntity;
use Entities\UrlEntity;
use Entities\UrlEntityForTask;
use Helpers\UrlModifier;
use Helpers\UrlValidator;
use Interfaces\IReturnable;

class ParserService implements IReturnable
{
    protected $pageEntity;
    protected $availableUrls;
    protected $urlEntityForTask;

    public function __construct(PageEntity $page, UrlEntityForTask $urlEntityForTask)
    {
        $this->pageEntity = $page;
        $this->urlEntityForTask = $urlEntityForTask;
    }

    public function parseContent()
    {
        preg_match_all("/<a[^>]*href\s*=\s*'([^']*)'|" .
            '<a[^>]*href\s*=\s*"([^"]*)"' . "/is", $this->pageEntity->content, $match);
        for ($i = 1; $i < sizeof($match); $i++) {
            foreach ($match[$i] as $url) {
                $url = ($this->isValidUrlMatch($url)) ? $this->prepareUrl($url) : $url;
                $urlValidator = new UrlValidator($url, $this->urlEntityForTask);
                if ($urlValidator->validate()) {
                    $urlModifier = new UrlModifier($url);
                    $urlModifier->modify();
                    $this->availableUrls[] = new UrlEntity($urlModifier->url);
                }
            }
        }
    }

    #region helpers


    private function __isValidProtocol($url)
    {
        return (strpos($url, "http") === false);
    }

    private function __isValidTrimmedUrl($url)
    {
        return (trim($url) !== "");
    }

    private function isValidUrlMatch($url)
    {
        return ($this->__isValidProtocol($url) && $this->__isValidTrimmedUrl($url));
    }

    private function __removeSlashFromStart($url)
    {
        if (strpos($url, '/') === 0) {
            return substr($url, 1);
        } else {
            return $url;
        }
    }

    private function __removeSlashFromEnd($url)
    {
        return substr($url, 0, -1);
    }

    private function __removeDoubleDots($url)
    {
        return str_replace('..', '', $url);
    }

    private function __removeMultiSlash($url)
    {
        return preg_replace('~(\/+)~', '/', $url);
    }

    #endregion

    private function prepareUrl($url)
    {
        if ($url[0] == "/") {
            $url = $this->__removeSlashFromStart($url);
        } elseif ($url[0] == ".") {
            while ($url[0] != "/") {
                $url = $this->__removeSlashFromStart($url);
            }
            $url = $this->__removeSlashFromEnd($url);
        }
        $url = $this->__removeDoubleDots($url);
        $url = $this->__removeMultiSlash($url);

        $url = $this->__removeSlashFromStart($url);

        return $this->urlEntityForTask->getDomainAndProtocolWithSlash() . $url;
    }

    public function getResult()
    {
        // TODO: Implement getResult() method.
        return $this->availableUrls;
    }
}