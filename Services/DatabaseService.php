<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 12:03
 */

namespace Services;

use Entities\UrlEntity;
use Entities\UrlEntityForTask;
use Exceptions\IncorrectEntityInstanceException;
use http\Exception\InvalidArgumentException;
use Interfaces\IVoid;
use PDO;

class DatabaseService implements IVoid
{
    private $tableName;
    private $pdo;

    public function __construct()
    {
        $host = 'localhost';
        $db   = 'generator';
        $user = 'root';
        $pass = '';
        $charset = 'utf8';

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $user, $pass, $opt);
    }

    public function createTableForParse(UrlEntityForTask $url)
    {
        $domain = $this->getOnlyDomain($url->domain);
        $domain = str_replace('-', '_', $domain);
        $this->tableName = "generator.urls_" . $domain;
        $this->pdo->exec("DROP TABLE IF EXISTS " . $this->tableName);
        $this->pdo->exec("
            CREATE TABLE IF NOT EXISTS " . $this->tableName . "( 
            id INT (11) NOT NULL AUTO_INCREMENT,
            url VARCHAR (255),
            status ENUM ('new','working','success','error','exporting','exported','ignore'),
            PRIMARY KEY (id),
            UNIQUE KEY (url)
        )");
        try {
            $this->pdo->exec(
                "INSERT INTO " . $this->tableName . " (url, status) 
                VALUES('" . $url->getDomainAndProtocolWithSlash() . "', '" . UrlEntity::STATUS_NEW . "')"
            );
            $table_name_for_urls = explode('.', $this->tableName);
            $this->pdo->exec(
                "UPDATE parse_sitemap_tasks SET table_name_for_urls = '".$table_name_for_urls[1]."' WHERE id = ". $url->id
            );
        } catch (\Exception $exception) {
            //
        }
    }

    public function getUrl()
    {
        $url = $this->pdo->query("SELECT * FROM " . $this->tableName . " WHERE status = 'new' LIMIT 1")->fetch();
        $urlEntity = new UrlEntity($url['url'], $url['id'], $url['status']);
        $this->updateUrlStatus($urlEntity, UrlEntity::STATUS_WORKING);
        if ($url['url'] === null) {
            return false;
        } else {
            return $urlEntity;
        }
    }

    public function getUrlForTask()
    {
        $urlEntityForTaskArray = [];
        $urlForTask = $this->pdo->query("SELECT * FROM parse_sitemap_tasks WHERE status = 'new'");
        while ($urlObj = $urlForTask->fetch()) {
            $urlEntityForTaskArray[] = new UrlEntityForTask(
                $urlObj['id'],
                $urlObj['domain'],
                $urlObj['first_page'],
                $urlObj['protocol'],
                $urlObj['status'],
                $urlObj['table_name_for_urls']
            );
        }
        return $urlEntityForTaskArray;
    }

    public function updateUrlStatus(UrlEntity $urlEntity, $status)
    {
        $this->pdo->exec(
            "UPDATE " . $this->tableName . " SET status = '" . $status . "' WHERE id = '" . $urlEntity->id . "'"
        );
    }

    public function updateUrlStatusForTask($id, $status)
    {
        $this->pdo->exec(
            "UPDATE parse_sitemap_tasks SET status = '" . $status . "' WHERE id = '" . $id . "'"
        );
    }

    public function updateTableNameForUrls($id, $table_name)
    {
        $this->pdo->exec(
            "UPDATE parse_sitemap_tasks SET table_name_for_urls = '" . $table_name . "' WHERE id = '" . $id . "'"
        );
    }

    public function saveUrls($arrayUrls)
    {
        foreach ($arrayUrls as $url) {
            if ($url instanceof UrlEntity) {
                $status = UrlEntity::STATUS_NEW;
            } else {
                $status = UrlEntity::STATUS_ERROR;
            }
            try {
                $this->pdo->exec(
                    "INSERT INTO " . $this->tableName . " (url, status) VALUES('" . $url->url . "', '" . $status . "')"
                );
            } catch (\Exception $exception) {
                continue;
            }
        }
    }

    private function getOnlyDomain($url)
    {
        $url = explode('.', $url);
        $url = $url[0];
        return $url;
    }
}