<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 19.12.2018
 * Time: 11:49
 */
ini_set('memory_limit', '32M');

require_once 'vendor/autoload.php';

$db = new Generator\GenerateMediator();
$db->mediate();
