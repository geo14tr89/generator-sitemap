<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 08.01.2019
 * Time: 11:27
 */

namespace Helpers;

use Entities\UrlEntityForTask;

class UrlValidator
{
    const UNAVAILABLE_EXTENSIONS = [
        'javascript:',
        '.css',
        '.js',
        '.ico',
        '.jpg',
        '.png',
        '.jpeg',
        '.swf',
        '.gif',
        'filter',
        'login',
    ];

    public $url;
    public $urlEntityForTask;

    public function __construct($url, UrlEntityForTask $urlEntityForTask)
    {
        $this->url = strtolower($url);
        $this->urlEntityForTask = $urlEntityForTask;
    }

    public function validate()
    {
        if ($this->isNotEmptyUrl()) {
            if ($this->validateUnavailableExtensions()) {
                if ($this->isAbsolute()) {
                    return ($this->checkDomain());
                }
            }
        }
        return false;
    }

    public function isNotEmptyUrl()
    {
        return !empty($this->url);
    }

    public function checkDomain()
    {
        return (strpos($this->url, $this->urlEntityForTask->getDomainWithProtocol()) === 0);
    }

    public function isAbsolute()
    {
        return (strpos($this->url, 'http') === 0);
    }

    private function validateUnavailableExtensions()
    {
        foreach (self::UNAVAILABLE_EXTENSIONS as $needle) {
            if (strpos($this->url, $needle) !== false) {
                return false;
            }
        }
        return true;
    }
}
