<?php
/**
 * Created by PhpStorm.
 * User: Андрей Харьковой
 * Date: 08.01.2019
 * Time: 12:41
 */

namespace Helpers;

class UrlModifier
{
    const NEEDLES = [
        '#',
        'tel:',
        'mailto:',
    ];

    public $url;

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function modify()
    {
        $this->removeSpecialChars();
    }

    public function removeSpecialChars()
    {
        foreach (self::NEEDLES as $needle) {
            $pos = strpos($this->url, $needle);
            if ($pos !== false) {
                $this->url = substr($this->url, 0, $pos);
            }
        }
    }
}
